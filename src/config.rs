use std::path::Path;

use minijinja::context;

pub struct Config<'a> {
    pub title: String,
    pub base_url: String,
    pub description: String,
    pub lang: String,
    pub output_dir: Box<&'a Path>,
}

impl Config<'_> {
    pub fn new() -> Self {
        return Config {
            title: String::from("Eleos in space"),
            base_url: String::from("http://blog.eleos.space"),
            description: String::from("Hi, I'm Eleos! I scroll endlessly the internet and learn as much as I can about software development, sys admin, as well as various other things.
            You'll find here posts about my computers projects, rambling, and my other hobbies than computers.\n
            I mostly code in Python and Rust"),
            lang: String::from("en"),
            output_dir: Box::new(Path::new("output/")),
        };
    }

    pub fn ctx(&self) -> minijinja::value::Value {
        context! {
            title => self.title,
            base_url => self.base_url,
            description => self.description.replace("\n", "<br>"), // TERRIBLY WRONG
            lang => self.lang,
        }
    }
}