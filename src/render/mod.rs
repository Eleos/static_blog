use crate::article::{Article, ArticleParsingError};
use crate::config::Config;
use crate::datetime::DTime;

use std::path::Path;
use std::fs;
use std::result::Result;

use glob::glob;
use minijinja::value::Value;
use minijinja::{Environment, Source, context};

mod filters;

#[derive(thiserror::Error, Debug)]
pub enum BuildError {
    #[error("Error while parsing article\nCaused by:\n\t{0}")]
    ArticleError(#[source] ArticleParsingError),
    #[error("Error while managing file or folder: {0}")]
    FileError(String),
    #[error("Rendering error with given template : {0}")]
    RenderError(#[from] minijinja::Error),
    #[error("Unhandled I/O error\nCaused by:\n\t{0}")]
    IoError(#[from] anyhow::Error),
}

pub fn parse_articles_files(folder_name: &str) -> Result<Vec<Article>, BuildError> {
    let folder_path = Path::new(folder_name);
    if !folder_path.exists() || !folder_path.is_dir() {
        return Err(BuildError::FileError(format!("{} either does not exist or isn't a folder", folder_name)));
    }

    let mut results = Vec::new();
    for entry in glob(&format!("{}/*.md", folder_name)).unwrap() {
        match entry {
            Ok(path) => {
                let file_content = fs::read_to_string(path.clone())
                    .map_err(|e| BuildError::IoError(anyhow::Error::new(e)))?;
                match Article::parse_article(file_content) {
                    Ok(article) => results.push(
                        Article{ stem: Path::new(&path).file_stem().unwrap().to_str().unwrap().to_owned(), ..article }
                    ),
                    Err(e) => return Err(BuildError::ArticleError(e)),
                };
            },
            Err(e) => return Err(BuildError::IoError(anyhow::Error::new(e))),
        };
    }

    Ok(results)
}

pub fn create_env(templates_pathnames: Vec<&str>) -> Result<minijinja::Environment, BuildError> {
    let mut env: Environment = Environment::new();
    let mut source = Source::new(); // We're using Source, cause otherwise env will own the `content` variable
    for pathname in templates_pathnames {
        verify_template(pathname)?;
        let path = Path::new(pathname);
        let content = fs::read_to_string(path)
            .map_err(|e| BuildError::IoError(anyhow::Error::new(e)))?;
        source.add_template(pathname, &content)?;
    }

    // TODO : Add a filter of date formatting
    env.set_source(source);
    env.add_filter("format_date", filters::format_date);
    Ok(env)
}

pub fn build_pages(articles : &Vec<Article>, config: &Config, env: &Environment, template_pathname: &str) -> Result<(), BuildError> {
    let tmpl = env.get_template(template_pathname)?;

    for art in articles {
        let article_pathname = format!("{}/{}.html", config.output_dir.display(), &art.stem);

        let article_path = Path::new(&article_pathname);
        if article_path.is_dir() {
            return Err(
                BuildError::FileError(format!("{} is an existing directory in the {} folder, remove entirely it before continue", article_pathname, config.output_dir.display()))
            );
        }
        
        let article_ctx = context!{
            post => art.ctx(),
            config => config.ctx(),
        };
        let rendered_html = tmpl.render(article_ctx)?;
        // What if files are already existing ? For now let's just rewrite the file over 
        fs::write(&article_path, rendered_html)
            .map_err(|e| BuildError::IoError(anyhow::Error::new(e)))?;
    }

    Ok(())
}

pub fn build_index(articles: &Vec<Article>, config: &Config, env: &Environment, template_pathname: &str) -> Result<(), BuildError> {
    let index_output = format!("{}/index.html", config.output_dir.display());
    let tmpl = env.get_template(template_pathname).unwrap(); // Unwrap cause we know at this point the template is loaded

    let index_ctx = context! {
        posts => articles.iter().map(|a| a.ctx()).collect::<Vec<minijinja::value::Value>>(),
        config => config.ctx(),
    };
    let index_render = tmpl.render(index_ctx)?;
    fs::write(&index_output, index_render)
        .map_err(|e| BuildError::IoError(anyhow::Error::new(e)))?;

    Ok(())
}

pub fn build_feed(articles: &Vec<Article>, config: &Config, env: &Environment, template_pathname: &str) -> Result<(), BuildError> {
    let feed_output = format!("{}/feed.xml", config.output_dir.display());
    let tmpl = env.get_template(template_pathname).unwrap(); // Map error ?
    
    let build_date = DTime::now();

    let feed_ctx = context! {
        posts => articles.iter().map(|a| a.ctx()).collect::<Vec<Value>>(),
        config => config.ctx(),
        feed_url => format!("{}/feed.xml", config.base_url),
        last_updated => Value::from_object(build_date),
    };

    let feed_render = tmpl.render(feed_ctx)?;
    fs::write(&feed_output, feed_render)
        .map_err(|e| BuildError::IoError(anyhow::Error::new(e)))?;
    Ok(())
}

fn verify_template(template_pathname: &str) -> Result<(), BuildError> {
    let template_path = Path::new(template_pathname);
    if !template_path.exists()  {
        return Err(BuildError::FileError(format!("{} does not exist", template_pathname)));
    }
    if !template_path.is_file() {
        return Err(BuildError::FileError(format!("{} isn't a file", template_pathname)));
    }
    if template_path.extension().is_none() {
        return Err(BuildError::FileError(format!("{} isn't a valid file", template_pathname)));
    }

    let ext = template_path.extension().unwrap().to_str();
    if vec!["html", "xml"].iter().filter(|s| ext == Some(s)).next().is_none() {
        return Err(BuildError::FileError(format!("{} isn't a valid file", template_pathname)));
    }

    Ok(())
}

fn verify_output_dir(output_dirname: &str) -> Result<(), BuildError> {
    let output_path = Path::new(output_dirname);
    if output_path.is_file() {
        return Err(BuildError::FileError(format!("{} is an existing file", output_dirname)));
    }
    if !output_path.exists() {
        fs::create_dir_all(&output_path).unwrap(); // Can unwrap since the folder doesn't exist
    }

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parse_articles_test() {
        parse_articles_files("srcs").unwrap();
    }

    #[test]
    #[should_panic]
    fn parse_articles_panic_test() {
        parse_articles_files("not_existing_folder").unwrap();
    }

    #[test]
    fn create_env_test() {
        let env = create_env(vec!["templates/article.html", "templates/index.html"]).unwrap();
        assert!(env.get_template("templates/article.html").is_ok());
        assert!(env.get_template("templates/index.html").is_ok());
    }

    #[test]
    #[should_panic]
    fn create_env_panic_test() {
        create_env(vec!["donotexist/file.html"]).unwrap();
    }

    #[test]
    fn write_articles_test() {
        let articles = parse_articles_files("srcs").unwrap();
        let config = Config::new();
        let env = create_env(vec!["templates/article.html"]).unwrap();
        assert_eq!((), build_pages(&articles, &config, &env, "templates/article.html").unwrap());
        assert!(Path::new("output/test1.html").exists());
    }

    #[test]
    fn write_index_test() {
        let articles = parse_articles_files("srcs").unwrap();
        let config = Config::new();
        let env = create_env(vec!["templates/article.html", "templates/index.html"]).unwrap();
        assert_eq!((), build_index(&articles, &config, &env, "templates/index.html").unwrap());
        assert!(Path::new("output/index.html").exists());
    }

    #[test]
    fn write_feed_test() {
        let articles = parse_articles_files("srcs").unwrap();
        let config = Config::new();
        let env = create_env(vec!["templates/article.html", "templates/index.html", "templates/feed.xml"]).unwrap();
        assert_eq!((), build_feed(&articles, &config, &env, "templates/feed.xml").unwrap());
        assert!(Path::new("output/feed.xml").exists());
    }
}