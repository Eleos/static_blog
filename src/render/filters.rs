use crate::datetime::DTime;
use minijinja::value::Value;

pub fn format_date(value: Value, format_str: &str) -> String {
    let o_date = value.downcast_object_ref::<DTime>();
    match o_date {
        Some(date) => date.format(format_str),
        None => format!("{:?}", value),
    }
}