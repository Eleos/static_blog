use std::fmt;

use chrono::{NaiveDateTime, NaiveDate, DateTime, Local};
use minijinja::value::Object;

#[derive(Debug, Eq, PartialEq, PartialOrd, Ord, Clone)]
pub struct DTime {
    date_time: DateTime<Local>,
}

impl DTime {
    pub fn from_date_str(input: &str, format: &str) -> Result<Self, String> {
        let rdate = NaiveDate::parse_from_str(input, format);
        match rdate {
            Ok(date) => Ok(date.into()),
            Err(e) => Err(format!("Error while parsing date {} with format {}: {}", input, format, e)),
        }
    }

    pub fn now() -> Self {
        DTime { date_time: Local::now() }
    }

    pub fn format(&self, fmt: &str) -> String {
        format!("{}", self.date_time.format(fmt))
    }
}

impl fmt::Display for DTime {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

impl Object for DTime {}

impl From<NaiveDate> for DTime {
    fn from(nd: NaiveDate) -> Self {
        nd.and_hms_opt(0, 0, 0).unwrap().into()
    }
}

impl From<NaiveDateTime> for DTime {
    fn from(ndt: NaiveDateTime) -> Self {
        let binding = Local::now();
        let offset = binding.offset();
        DTime { date_time: DateTime::<Local>::from_utc(ndt, *offset) }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_format() {
        let dt = DTime::from(NaiveDate::from_ymd_opt(2022, 12, 13).unwrap());
        assert_eq!(dt.format("%Y %m %d"), "2022 12 13");
    }

    #[test]
    fn test() {
        let dt = DTime::from(NaiveDate::from_ymd_opt(2022, 12, 13).unwrap());
        eprintln!("{:?}", dt.date_time);
    }
}