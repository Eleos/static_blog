use std::{cmp::Ordering, collections::HashMap};
use std::convert::From;

use minijinja::value::Value;
use regex::Regex;
use minijinja::context;
use thiserror;
use crate::datetime::DTime;

extern crate markdown;

#[derive(Debug, PartialEq, Eq)]
pub struct MetaData {
    pub title: String,
    pub date: DTime,
}

#[derive(thiserror::Error, Debug, PartialEq)]
pub enum ArticleParsingError {
    #[error("{0}")]
    MetadataParsingError(String),
    #[error("{0}")]
    ArticleFormatError(String)
}

impl MetaData {
    fn from_frontmatter(input: &str) -> Result<MetaData, ArticleParsingError> {
        let hmap_input = input.split("\n")
            .filter_map(|line| line.split_once(":"))
            .map(|(mt_str, value)| (mt_str.trim().to_lowercase().as_str().into(), value.trim()))
            .filter(|(mt, value): &(MetaDataType, &str)| *mt != MetaDataType::Unknown && mt.verify(*value))
            .collect::<HashMap<MetaDataType, &str>>();

        // Verifying that title and date tags are in the hashmap
        for mdt in vec![MetaDataType::Title, MetaDataType::Date] {
            if !hmap_input.contains_key(&mdt) {
                return Err(ArticleParsingError::MetadataParsingError(
                    format!("Metadatas input does not contains valid {} tag", mdt)
                ));
            }
        }

        // Verifying that the date is valid and not out of bound
        let date = DTime::from_date_str(hmap_input.get(&MetaDataType::Date).unwrap(), "%Y-%m-%d")
            .map_err(|err| ArticleParsingError::MetadataParsingError(err))?;

        Ok(MetaData { 
            title: hmap_input.get(&MetaDataType::Title).unwrap().to_string(),
            date,
        })
    }
}

impl Ord for MetaData {
    fn cmp(&self, other: &Self) -> Ordering {
        self.date.cmp(&other.date)
    }
}

impl PartialOrd for MetaData {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Eq, PartialEq, Hash)]
enum MetaDataType {
    Title,
    Date,
    Unknown
}

impl MetaDataType {
    fn verify(&self, input: &str) -> bool {
        self.regex_pattern().captures(input).is_some()
    }

    fn regex_pattern(&self) -> Regex {
        Regex::new(match self {
            MetaDataType::Title => r"[^\n]",
            MetaDataType::Date => r"\d{4}-\d{2}-\d{2}",
            MetaDataType::Unknown => r"",
        }).unwrap() // We can unwrap safely as our regex are valid
    }
}

impl std::fmt::Display for MetaDataType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            MetaDataType::Title => write!(f, "title"),
            MetaDataType::Date => write!(f, "date"),
            MetaDataType::Unknown => write!(f, ""),
        }
    }
}

impl From<&str> for MetaDataType {
    fn from(item: &str) -> Self {
        match item {
            "title" => MetaDataType::Title,
            "date" => MetaDataType::Date,
            _ => MetaDataType::Unknown
        }
    }
}

#[derive(Debug, PartialEq, Eq, Ord, PartialOrd)]
pub struct Article {
    pub mdata: MetaData,
    pub article_html: String,
    pub stem: String,
}

impl Article {
    pub fn parse_article(text: String) -> Result<Article, ArticleParsingError> {
        let elements = text.split("---\n").collect::<Vec<&str>>();
        if elements.len() < 3 {
            return Err(
                ArticleParsingError::ArticleFormatError("Article should have it's metadatas splitted by 2 \"---\\n\"".to_owned())
            );
        }

        // elements[0] should be empty (or will just be ignored), the metadatas are contained between "---"
        let mdata = MetaData::from_frontmatter(elements[1])?;
        let article_html = Article::parse_markdown(&elements[2..].join("---\n"));
        Ok(Article { mdata, article_html, stem: String::new() })
    }

    pub fn ctx(&self) -> minijinja::value::Value {
        context! {
            title => self.mdata.title,
            date => Value::from_object(self.mdata.date.clone()),
            stem => format!("{}.html", self.stem),
            content => self.article_html,
        }
    }

    fn parse_markdown(text: &str) -> String {
        markdown::to_html(text)
    }
}


#[cfg(test)]
mod test {
    use super::*;
    use chrono::NaiveDate;

    #[test]
    fn parse_markdown_test() {
        let text = "Content";
        assert_eq!(Article::parse_markdown(text), "<p>Content</p>\n");
    }

    #[test]
    fn frontmatter_builder_test() {
        let input = "title: foo\ndate: 2022-12-13";
        let expected_output = MetaData {
            title: "foo".to_string(),
            date: DTime::from(NaiveDate::from_ymd_opt(2022, 12, 13).unwrap())
        };
        assert_eq!(MetaData::from_frontmatter(input).unwrap(), expected_output);
    }

    #[test]
    fn frontmatter_builder_test_more_complex_name() {
        let input = "title: foo bar\ndate: 2022-12-13";
        let expected_output = MetaData {
            title: "foo bar".to_string(),
            date: DTime::from(NaiveDate::from_ymd_opt(2022, 12, 13).unwrap())
        };
        assert_eq!(MetaData::from_frontmatter(input).unwrap(), expected_output);
    }

    #[test]
    #[should_panic]
    fn frontmatter_builder_panic_test() {
        let false_input = "title: foo\ndate: 9 September";
        MetaData::from_frontmatter(false_input).unwrap();
    }

    #[test]
    fn parse_article_test() {
        let input = "---
        title: foo bar
        date: 2022-12-13
        ---
Content".to_owned();
        let expected_output = Ok(Article {
            mdata: MetaData {
                title: "foo bar".to_owned(),
                date: DTime::from(NaiveDate::from_ymd_opt(2022, 12, 13).unwrap())
            },
            article_html: "<p>Content</p>\n".to_owned(),
            stem: String::new(),
        });
        assert_eq!(Article::parse_article(input), expected_output);
    }

    #[test]
    #[should_panic]
    fn parse_article_panic_test() {
        let wrong_input = "Content".to_owned();
        Article::parse_article(wrong_input).unwrap();
    }
}