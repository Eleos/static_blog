pub mod article;
pub mod config;
mod render;
pub mod datetime;

use crate::render::{create_env, build_index, build_pages, build_feed, parse_articles_files};
use crate::config::Config;
use std::process::exit;
use std::collections::HashMap;

const SOURCE_FOLDERNAME: &str = "srcs";
const TEMPLATE_FOLDER: &str = "templates";
const OUTPUT_FOLDER: &str = "output";

fn main() {
    let config = Config::new();

    let mut articles = parse_articles_files(SOURCE_FOLDERNAME).unwrap_or_else(|err| {
        eprintln!("Error while parsing articles in {}:", SOURCE_FOLDERNAME);
        eprintln!("{}", err);
        exit(1);
    });

    articles.sort_by(|a, b| b.cmp(a)); // Reverse sort the articles, the first in the vec will be the more recent. TODO: Use the unsafe version for faster sort ?

	let template_hmap: HashMap<String, String>  = HashMap::from([
        ("article_temp".to_owned(), format!("{}/article.html", TEMPLATE_FOLDER)),
        ("index_temp".to_owned(), format!("{}/index.html", TEMPLATE_FOLDER)),
        ("feed_temp".to_owned(), format!("{}/feed.xml", TEMPLATE_FOLDER)),
    ]);
    let env = create_env(template_hmap.values().map(|s| s.as_str()).collect::<Vec<&str>>()).unwrap_or_else(|err| {
        eprintln!("Error while building template environnement :");
        eprintln!("{}", err);
        exit(1);
    });

    build_pages(&articles, &config, &env, &template_hmap["article_temp"]).unwrap_or_else(|err| {
        eprintln!("Error while building articles :");
        eprintln!("{}", err);
        exit(1);
    });

    build_index(&articles, &config, &env, &template_hmap["index_temp"]).unwrap_or_else(|err| {
        eprintln!("Error while building index :");
        eprintln!("{}", err);
        exit(1);
    });

    build_feed(&articles, &config, &env, &template_hmap["feed_temp"]).unwrap_or_else(|err| {
        eprintln!("Error while building rss feed :");
        eprintln!("{}", err);
        exit(1);
    });
}
